(function(){
  'use strict';

  function routeConfig($stateProvider, $urlRouterProvider){
    $stateProvider
      .state('root', {
        url: '',
        abstract: true,
        views: {
          'loadingMask': {
            templateUrl: '/app/modules/screenLocker/components/ScreenLocker/loadingMask.html',
            controller: 'ScreenLockerController',
            controllerAs: 'lockedScreen'
          },
          'header': {
            templateUrl: '/app/modules/layout/components/header/header.html',
            controller: 'HeaderController',
            controllerAs: 'header'
          },
          'footer': {
            templateUrl: '/app/modules/layout/components/footer/footer.html'
          }
        }
      })
      .state('404', {
        url: '/404',
        templateUrl: '404.tpl.html'
      });
    $urlRouterProvider.when('', '/character');
    $urlRouterProvider.otherwise('/404');
  }

  angular
  .module('chApp')
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    routeConfig
  ]);
})();