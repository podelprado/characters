(function(){
  'use strict';

  function AppConfig($locationProvider){
    $locationProvider.hashPrefix('');
  }

  angular
    .module(ModuleName)
    .config(['$locationProvider', AppConfig]);
})();