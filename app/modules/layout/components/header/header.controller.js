(function(){
  'use strict';

  function HeaderController($scope){
    this.title = 'Characters';

  }

  angular
    .module('layout')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = [
    '$scope'
  ];
})();
