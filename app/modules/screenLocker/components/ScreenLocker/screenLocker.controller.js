(function(){
  'use strict';

  function ScreenLockerController($rootScope, ScreenLockerService){
    $rootScope.$on('LISTSTART', function(){
      ScreenLockerService.openWindowsLoading();
    });
    $rootScope.$on('LISTEND', function(){
      ScreenLockerService.closeWindowsLoading();
    });
    $rootScope.$on('SHOW_LOADING', function(event, data){
      this.isLoading = data.locked;
    }.bind(this));
  }

  angular.module('screenLocker').controller('ScreenLockerController', ScreenLockerController);

  ScreenLockerController.$inject = ['$rootScope', 'ScreenLockerService'];
})();
