(function(){
  'use strict';

  function ScreenLockerService($rootScope){
    var setIsLoading = function(value){
      $rootScope.$emit('SHOW_LOADING', {locked: value});
    };
    var openWindows = function(){
      setIsLoading(true);
    };
    var closeWindows = function(){
      setIsLoading(false);
    };

    return {
      openWindowsLoading: function(){
        openWindows();
      },
      closeWindowsLoading: function(){
        closeWindows();
      }
    };
  }

  angular
    .module('screenLocker')
    .factory('ScreenLockerService', ScreenLockerService);

  ScreenLockerService.$inject = [
    '$rootScope'
  ];
})();