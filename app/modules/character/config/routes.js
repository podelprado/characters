(function(){
  'use strict';

  angular
  .module('character')
  .config([
    '$stateProvider',
    function($stateProvider){
      $stateProvider
      .state('root.character', {
        url: '/character',
        views: {
          'container@': {
            templateUrl: 'app/modules/character/components/character/character.html',
            controller: 'CharacterController',
            controllerAs: 'character'
          }
        }
      });
    }
  ]);
})();