angular
  .module('character')
  .controller('CharacterController', function($rootScope){
    this.listFilms = false;
    $rootScope.$emit('LISTSTART', '');


    this.characterFilms = function(films){
      this.listFilms = films;
    };
  })
  .component('characterMain', {
    controller: 'CharacterController',
    controllerAs: 'characterCtrl'
  });
