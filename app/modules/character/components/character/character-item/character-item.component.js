angular
  .module('character')
  .component('characterItem', {
    templateUrl: 'app/modules/character/components/character/character-item/character-item.html',
    bindings: {
      data: '<',
      characterFilms: '&'
    }
  });