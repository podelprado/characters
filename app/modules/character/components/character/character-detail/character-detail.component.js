angular
  .module('character')
  .controller('CharacterDetailController', function($q, $http, $rootScope, $scope){
    this.$onChanges = function() {
      $rootScope.$emit('SHOW_LOADING', {locked: true});
      this.promises = [];
      this.filmsByCharacters = [];
      _.each(this.data, function(element){
        this.promises.push($http.get('https://swapi.co/api/films/' + element.split('/')[5]));
      }.bind(this));
      $q.all(this.promises).then(function(data){
        $rootScope.$emit('SHOW_LOADING', {locked: false});
        this.filmsByCharacters = data;
      }.bind(this));
    };
  })
  .component('characterDetail', {
    templateUrl: 'app/modules/character/components/character/character-detail/character-detail.html',
    controller: 'CharacterDetailController',
    controllerAs: 'detailCtrl',
    bindings: {
      data: '<'
    }
  });
