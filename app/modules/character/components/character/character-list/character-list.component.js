angular
  .module('character')
  .controller('CharacterListController', function($rootScope, $http){
    this.listCharacters = {};
    this.currentPage = 0;
    this.pageSize = 10;
    this.pages = [];

    this.getCharacters = function(id){
      $http({
        method: 'GET',
        url: 'https://swapi.co/api/people/',
        params: {page: id}
      }).then(function successCallback(response) {
        this.listCharacters = response.data;
        $rootScope.$emit('LISTEND', '');
        this.configPages();
      }.bind(this), function errorCallback(response) {
        console.log('Error: ', response);
      });
    };

    this.configPages = function() {
      this.pages.length = 0;
      var ini = this.currentPage - 4;
      var fin = this.currentPage + 5;
      if (ini < 1) {
        ini = 1;
        if (Math.ceil(this.listCharacters.count / this.pageSize) > 10){
          fin = 10;
        }
        else{
          fin = Math.ceil(this.listCharacters.count / this.pageSize);
        }
      } else {
        if (ini >= Math.ceil(this.listCharacters.count / this.pageSize) - 10) {
          ini = Math.ceil(this.listCharacters.count / this.pageSize) - 10;
          fin = Math.ceil(this.listCharacters.count / this.pageSize);
        }
      }
      if (ini < 1) ini = 1;
      for (var i = ini; i <= fin; i++) {
        this.pages.push({
          no: i
        });
      }

      if (this.currentPage >= this.pages.length)
        this.currentPage = this.pages.length - 1;
    };

    this.setPage = function(index) {
      $rootScope.$emit('LISTSTART', '');
      this.getCharacters(index);
      this.currentPage = index - 1;
    };

    this.getCharacters(1);
  })
  .filter('startFromGrid', function() {
    return function(input, start) {
      start = +start;
      return input.slice(start);
    }
  })
  .component('characterList', {
    templateUrl: 'app/modules/character/components/character/character-list/character-list.html',
    controller: 'CharacterListController',
    controllerAs: 'characterCtrl',
    bindings: {
      characterFilms: '&'
    }
  });

