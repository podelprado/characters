(function(){
  'use strict';

  angular
  .module('character', [
    'ui.router'
  ])
  .config([
    '$logProvider',
    function($logProvider){
      $logProvider.debugEnabled(false);
    }
  ]);
})();