var ModuleName = 'chApp';

(function(){
  'use strict';

  angular
    .module(ModuleName, [
      'ngResource',
      'ui.bootstrap',
      'ui.router',
      'character',
      'layout',
      'screenLocker'
    ]);
})();

